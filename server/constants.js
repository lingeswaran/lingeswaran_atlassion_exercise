const constants = {
    "ERR500": {
        "details": "Something went wrong!"
    },
    "ERR404":{
        "details": "No data found!"
    }
}

module.exports = constants;