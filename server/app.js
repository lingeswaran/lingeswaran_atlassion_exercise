const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    return next();
  });
  
let spaces = require('./API/space');
let users = require('./API/users');
let constants = require('./constants');
let error_500 = constants.ERR500;
let error_404 = constants.ERR404;


app.use(bodyParser.json())

app.use("/space", spaces);
app.use("/users", users);

  // Handle 404
  app.use(function(req, res) {
    res.status(404);
    res.json(error_404);
 });
 
 // Handle 500
 app.use(function(error, req, res, next) {
    res.status(500);
    res.json(error_500);
 });

let port = 5000;
app.listen(port, () => {
    console.log(`Server started at port: ${port}`);
});