let express = require('express');
let router = express.Router();

let constants = require('../constants');
let error_500 = constants.ERR500;
let error_404 = constants.ERR404;

let authors = {
    "sys": {
        "type": "Array"
    },
    "total": 1,
    "skip": 0,
    "limit": 100,
    "items": [{
        "fields": {
            "name": "Alana Atlassian",
            "role": "Author"
        },
        "sys": {
            "id": "4FLrUHftHW3v2BLi9fzfjU",
            "type": "User"
        }
    }, {
        "fields": {
            "name": "John Doe",
            "role": "Editor"
        },
        "sys": {
            "id": "4FLrUHftHW3v2BLi9fzfjU2",
            "type": "User"
        }
    }]
};

router.get("/", (req, res) => {
    try {
        if (authors.hasOwnProperty("items") && authors.items.length > 0) {
            res.send(authors)
        } else {
            res.status(404);
            res.json(error_404);
        }
    } catch (error) {
        res.status(500);
        res.json(error_500)
    }
});

router.get("/:userId", (req, res) => {
    try {
        let userId = req.params.userId;
        if (authors.hasOwnProperty("items") && authors.items.length > 0) {
            let items = authors.items.filter((obj) => {
                return obj.sys.id == userId;
              });
            res.send(items[0]);
        } else {
            res.status(404);
            res.json(error_404);
        }
    } catch (error) {
        res.status(500);
        res.json(error_500)
    }
});

module.exports = router;