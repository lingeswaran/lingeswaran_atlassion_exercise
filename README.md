## Used Technologies 
Client side: React Js
Server side: Node Js

## To run react app

In the project directory, you can run:
### `npm install`

Install project dependencies
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.


## To run server(API)
Open another terminal and target project folder
### `node server/app.js`
API running on http://localhost:5000
