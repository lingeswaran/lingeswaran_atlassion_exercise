import fetch from 'node-fetch';

const URL = "http://localhost:5000";
const SpacesHttp = {
    getSpaces: () => {
        return fetch(`${URL}/space`);
    },
    getSpaceDetails: (id) => {
        return fetch(`${URL}/space/${id}`);
    },
    getAutorById: (id) => {
        return fetch(`${URL}/users/${id}`);
    },
    getEntriesBySpaceId: (id) => {
        return fetch(`${URL}/space/${id}/entries`);
    },
    getAssetsBySpaceId: (id) => {
        return fetch(`${URL}/space/${id}/assets`);
    },
    getAutors: (id) => {
        return fetch(`${URL}/users`);
    },
}
export default SpacesHttp;