import React, { Component } from 'react';
import SpacesHttp from '../services/httpService';
import {
    Link
} from 'react-router-dom';

import Details from './spaceDetails';

class Spaces extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false,
            details: [],
            message: ""
        }
    }
    getSpaces() {
        SpacesHttp.getSpaces()
            .then((res) => {
                if (!res.ok) {
                    this.setState({ error: true })
                }
                return res.json()
            })
            .then((json) => {
                if (!this.state.error) {
                    this.setState({ details: json.items });
                }
                else {
                    this.setState({ details: json.details });
                }
            })
            .catch((err) => {
                this.setState({ error: true, message: "Something went wrong!" });
            });
    }
    componentDidMount() {
        this.getSpaces();
    }
    renderSpaces() {
        let state = this.state;
        let template;
        if (!state.error) {
            template = state.details.map((obj, index) => {
                return <div className="col-md-12 col-xs-12 col-sm-12 col-lg-12" key={index}>
                    <h3><Link to={`/spaceexplorer/${obj.sys.id}`}>{obj.fields.title}</Link></h3>
                    <hr />
                </div>
            });
        } else {
            template = <div className="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                {state.details}
            </div>
        }
        return template;
    }
    checkRedirection() {
        if (this.props.match.params.spaceId === undefined) {
            if (this.state.details.length > 0) {
                window.location.href = `/spaceexplorer/${this.state.details[0].sys.id}`;
            }
        }
    }
    render() {
        let template;
        if (this.state.error) {
            template = <div className="row">
                <div className="alert alert-danger">{this.state.message}</div></div>
        } else {
            template = <div className="row">
                {this.checkRedirection()}
                <div className="col-md-4 col-sm-4 col-xs-12 col-lg-3">
                    {this.renderSpaces()}
                </div>
                <div className="col-md-8 col-sm-8 col-xs-12 col-lg-9">
                    <Details match={this.props.match} />
                </div>
            </div>
        }
        return (
            <div>
                {template}
            </div>

        );
    }
}
export default Spaces;