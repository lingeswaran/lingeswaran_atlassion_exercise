import React, { Component } from 'react';

class Entries extends Component {
    getAuthorName(id) {
        let tempAuthor = this.props.authors.filter((obj) => {
            return obj.sys.id === id;
        });
        return tempAuthor.length > 0 ? tempAuthor[0].fields.name : '';
    }
    renderTable() {
        let data = this.props.data;
        let template;
        if (!data.error) {
            template = data.items.map((obj, index) => {
                return <tr key={index}>
                    <td>{obj.fields.title}</td>
                    <td>{obj.fields.summary}</td>
                    <td>{this.getAuthorName(obj.sys.createdBy)}</td>
                    <td>{this.getAuthorName(obj.sys.updatedBy)}</td>
                    <td>{obj.sys.updatedAt}</td>
                </tr>
            });
        }else{
            template = <tr><td colSpan="5">{data.items}</td></tr>
        }
        return template;
    }
    render() {
        return (
            <table className="table table-hover table-strips">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        <th>Last Updated</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTable()}
                </tbody>
            </table>
        );
    }
}
export default Entries;