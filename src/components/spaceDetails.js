import React, { Component } from 'react';
import SpacesHttp from '../services/httpService';
import Entries from './entries';
import Assets from './assets';

class Details extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "spaceId": props.match.params.spaceId,
            "error": true,
            "message": "",
            "details": {},
            "author": {},
            "entries": {
                "error": false,
                "items": []
            },
            "assets": {
                "error": false,
                "items": []
            },
            "allAuthors": []
        }
    }

    getAuthorById(id) {
        SpacesHttp.getAutorById(id)
            .then((res) => {
                return res.json()
            })
            .then((json) => {
                this.setState({ author: json.fields });
            })
            .catch((err) => {
                this.setState({ error: true, details: err.details });
            });
    }

    getEntriesBySpaceId() {
        let entries = this.state.entries;
        SpacesHttp.getEntriesBySpaceId(this.state.spaceId)
            .then((res) => {
                if (!res.ok) {
                    entries.error = true;
                    this.setState({ entries: entries });
                }
                return res.json()
            })
            .then((json) => {
                if (!entries.error) {
                    entries.error = false;
                    entries.items = json.items;
                    this.setState({ entries: entries });
                }
                else {
                    entries.error = true;
                    entries.items = json.details;
                    this.setState({ entries: entries });
                }
            })
            .catch((err) => {
                entries.error = true;
                entries.items = err.details;
                this.setState({ entries: entries });
            });
    }

    getAssetsBySpaceId() {
        let assests = this.state.assets;
        SpacesHttp.getAssetsBySpaceId(this.state.spaceId)
            .then((res) => {
                if (!res.ok) {
                    assests.error = true;
                    this.setState({ assests: assests });
                }
                return res.json()
            })
            .then((json) => {
                if (!assests.error) {
                    assests.error = false;
                    assests.items = json.items;
                    this.setState({ assests: assests });
                }
                else {
                    assests.error = true;
                    assests.items = json.details;
                    this.setState({ assests: assests });
                }
            })
            .catch((err) => {
                assests.error = true;
                assests.items = err.details;
                this.setState({ assests: assests });
            });
    }

    getAllAthors() {
        SpacesHttp.getAutors()
            .then((res) => {
                return res.json()
            })
            .then((json) => {
                this.setState({ allAuthors: json.items });
            });
    }

    getSpaces(id) {
        SpacesHttp.getSpaceDetails(id)
            .then((res) => {
                if (res.ok) {
                    this.setState({ error: false })
                } else {
                    this.setState({ error: true })
                }
                return res.json()
            })
            .then((json) => {
                if (!this.state.error) {
                    console.log(json)
                    this.setState({ details: json.fields });
                    this.getAuthorById(json.sys.createdBy);
                    this.getEntriesBySpaceId();
                    this.getAssetsBySpaceId();
                    this.getAllAthors();
                }
                else {
                    this.setState({ error: true, message: json.details });
                }
            })
            .catch((err) => {
                this.setState({ error: true, message: "Something went wrong!" });
            });
    }
    componentWillReceiveProps(newProps) {
        let id = newProps.match.params.spaceId;
        this.setState({
            "spaceId": id,
            "error": false,
            "details": {},
            "author": {},
            "entries": {
                "error": false,
                "items": []
            },
            "assets": {
                "error": false,
                "items": []
            },
            "allAuthors": []
        });
        this.getSpaces(id);

    }
    componentDidMount() {
        if (this.props.spaceId !== undefined) {
            this.getSpaces(this.props.spaceId);
        }
    }

    render() {
        let template;
        if (this.state.error) {
            template = <div className="row">
                <div className="alert alert-danger">{this.state.message}</div></div>
        } else {
            template = <div>
                <div className="form-inline">
                    <span className="title">{this.state.details.title}</span>&nbsp;&nbsp;
                    <span>Created by {this.state.author.name}</span>
                </div>
                <textarea className="form-control" value={this.state.details.description} ></textarea>
                <ul className="nav nav-tabs">
                    <li className="active"><a data-toggle="tab" href="#entries">Entries</a></li>
                    <li><a data-toggle="tab" href="#assets">Assets</a></li>
                </ul>

                <div className="tab-content">
                    <div id="entries" className="tab-pane fade in active">
                        <Entries data={this.state.entries} authors={this.state.allAuthors} />
                    </div>
                    <div id="assets" className="tab-pane fade">
                        <Assets data={this.state.assets} authors={this.state.allAuthors} />
                    </div>
                </div>
            </div>
        }
        return (
            <div>
                {template}
            </div>
        );
    }
}
export default Details;