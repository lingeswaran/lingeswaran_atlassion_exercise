import React, { Component } from 'react';
import Spaces from './components/spaces';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Switch>
            <Route exact path="/spaceexplorer" component={Spaces} />
            <Route exact path="/spaceexplorer/:spaceId" component={Spaces} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
